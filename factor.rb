require 'sinatra'
require 'open-uri'
require 'nokogiri'

get '/:num' do
	num = params[:num]
	appid = ENV['APPID']
	url = "http://api.wolframalpha.com/v2/query?input=factor+#{num}&appid=#{appid}"
	xml = Nokogiri(open(url, &:read))
	(xml / 'pod[title="Prime factorization"] plaintext').text
end
